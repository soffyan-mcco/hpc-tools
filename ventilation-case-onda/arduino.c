#include <EEPROM.h>

const byte OC1A_PIN = 9;
const byte OC1B_PIN = 10;

int newVal;

const word PWM_FREQ_HZ = 25000;
const word TCNT1_TOP = 16000000/(2*PWM_FREQ_HZ);

void setup() {
  newVal = EEPROM.read(69);
  if (newVal == 255) {
    EEPROM.write(69, 50);
  }
  setPwmDuty(newVal);
  pinMode(OC1A_PIN, OUTPUT);
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;

  TCCR1A |= (1 << COM1A1) | (1 << WGM11);
  TCCR1B |= (1 << WGM13) | (1 << CS10);
  ICR1 = TCNT1_TOP;
  Serial.begin(9600);
}

void loop() {
  int getPwm;
  int currentEEPROM;
  if (Serial.available() > 0) {
        getPwm = Serial.parseInt();
        if (getPwm == 112) {
          currentEEPROM = EEPROM.read(69);
          Serial.println(currentEEPROM);
        } else {
          Serial.println(getPwm);
          newVal = getPwm;
          if (newVal >= 20 && newVal <= 100) {
            setPwmDuty(newVal);
            Serial.println("Success");
            EEPROM.write(69, newVal);
          } else {
            Serial.println("Value bust be between 25 and 100");
          }
       }
    }
}

void setPwmDuty(byte duty) {
  OCR1A = (word) (duty*TCNT1_TOP)/100;
}