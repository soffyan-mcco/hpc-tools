#!/bin/sh
# setfanspd.sh

stty -F /dev/ttyUSB0 9600
cat /dev/ttyUSB0 &
bgPid=$!

if { [ $1 -ge 20 ] && [ $1 -le 100 ]; }
then
        echo $1 >/dev/ttyUSB0
        echo "success"
else
        echo "must be between 20 and 100"
fi

kill $bgPid